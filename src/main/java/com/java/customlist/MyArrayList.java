package com.java.customlist;

public class MyArrayList<T> implements MyList<T> {
    private final int INITIALIZATION = 16;
    private Object[] array = new Object[INITIALIZATION];
    private int size = 0;

    @Override
    public void add(T elem) {
        if (size == array.length - 1)
            resize(array.length * 2);
        array[size++] = elem;
    }

    @Override
    public void insert(T elem, int index) {
        size += 1;
        Object[] temp = array;
        array = new Object[size];
        if (index >= 0) System.arraycopy(temp, 0, array, 0, index);
        array[index] = elem;
        if (size - (index + 1) >= 0) System.arraycopy(temp, index, array, index + 1, size - (index + 1));
    }

    @Override
    public void remove(int index) {
        if (size - index >= 0) System.arraycopy(array, index + 1, array, index, size - index);
        array[size] = null;
        size--;
    }

    @Override
    public void remove(T value) {
        int index = 0;
        for (int i = 0; i < size; i++) {
            if (array[i].equals(value)) {
                array[i] = null;
                index = i;
            }
        }
        if (size - index >= 0) System.arraycopy(array, index + 1, array, index, size - index);
        size--;

    }

    @Override
    public int indexOf(T value) {
        int result = -1;
        for (int i = 0; i < size; i++) {
            if (array[i].equals(value)) {
                result = i;
            }
        }
        return result;
    }

    @Override
    public boolean contains(T value) {
        boolean statement = false;
        for (int i = 0; i < size; i++) {
            if (array[i].equals(value)) {
                statement = true;
                break;
            }
        }
        return statement;
    }

    @Override
    public boolean containsAll(MyList<T> values) {
        boolean statement = false;
        int count = 0;
        if (values.size() == size) {
            for (int i = 0; i < values.size(); i++) {
                if (values.get(i).equals(array[i])) {
                    count++;
                }
            }
        }
        if (count == size) {
            statement = true;
        }
        return statement;
    }

    @Override
    public T get(int index) {
        return (T) array[index];
    }

    @Override
    public void set(int index, T value) {
        array[index] = value;
    }

    @Override
    public int size() {
        return size;
    }

    private void resize(int newLength) {
        Object[] newArray = new Object[newLength];
        System.arraycopy(array, 0, newArray, 0, size);
        array = newArray;
    }
}
