package com.java.customlist;

import java.util.*;

public class MyHashMap<K,V> implements Map<K,V> {

    private final static int DEFAULT_CAPACITY = 16;

    private static class Node<K,V> implements Map.Entry<K,V> {
        V value;
        K key;
        Node<K,V> next;

        public Node(K key, V value) {
            this.key = key;
            this.value = value;
        }

        boolean hasValue(Object value) {
            return this.value.equals(value);
        }

        boolean hasKey(Object key) {
            return this.key.equals(key);
        }

        @Override
        public K getKey() {
            return key;
        }

        @Override
        public V getValue() {
            return value;
        }

        @Override
        public V setValue(V value) {
            return this.value = value;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Node<?, ?> node = (Node<?, ?>) o;
            return Objects.equals(value, node.value) && Objects.equals(key, node.key) && Objects.equals(next, node.next);
        }

        @Override
        public int hashCode() {
            return Objects.hash(value, key, next);
        }
    }

    private Node<K,V>[] nodes;
    private int size;
    private final int capacity;

    public MyHashMap(int capacity) {
        this.capacity = capacity;
        this.nodes = new Node[capacity];
    }

    public MyHashMap() {
        this(DEFAULT_CAPACITY);
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean containsKey(Object key) {
        if (key == null) return false;
        int index = getIndex(key);
        for (Node<K,V> current = nodes[index]; current != null; current = current.next) {
            if (current.hasKey(key)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean containsValue(Object value) {
        for (int i = 0; i < capacity; i++) {
            for (Node<K,V> current = nodes[i]; current != null; current = current.next) {
                if (current.hasValue(value)) {
                    return true;
                }
            }
        }
        return false;
    }

    private int getIndex(Object o) {
        return Math.abs(o.hashCode() % nodes.length);
    }

    @Override
    public V get(Object key) {
        if (key == null) throw new UnsupportedOperationException();
        int index = getIndex(key);
        for (Node<K,V> current = nodes[index]; current != null; current = current.next) {
            if (current.hasKey(key)) {
                return current.getValue();
            }
        }
        return null;
    }

    @Override
    public V put(K key, V value) {
        if (key == null) throw new UnsupportedOperationException();
        int index = getIndex(key);

        Node<K,V> node = new Node<>(key, value);

        if (nodes[index] == null) {
            nodes[index] = node;
            size++;
            return value;
        }

        for (Node<K,V> current = nodes[index]; current != null; current = current.next) {
            if (current.hasKey(key)) {
                size++;
                return current.setValue(value);
            } else if (current.next == null) {
                current.next = node;
                size++;
                break;
            }
        }
        return value;
    }

    @Override
    public V remove(Object key) {
        if (key == null) throw new UnsupportedOperationException();
        int index = getIndex(key);
        V value = null;
        for (Node<K,V> current = nodes[index]; current != null; current = current.next) {
            if (current.hasKey(key)) {
                value = current.getValue();
                current.setValue(null);
                size--;

            }
        }
        return value;
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {

        List<K> listKey = new ArrayList<>();
        List<V> listValue = new ArrayList<>();

        for (Entry<? extends K,? extends V> some : m.entrySet()) {
            listKey.add(some.getKey());
            listValue.add(some.getValue());
        }

        for (int i = 0; i < listKey.size(); i++) {
            Node<K,V> node = new Node<>(listKey.get(i), listValue.get(i));
            if (nodes[i] == null) {
                nodes[i] = node;
            }
        }
        size = listKey.size();
    }

    @Override
    public void clear() {
        this.nodes = new Node[capacity];
        size = 0;
    }

    @Override
    public Set<K> keySet() {
        Set<K> result = new HashSet<>(capacity);
        for (int i = 0; i < capacity; i++) {
            for (Node<K,V> current = nodes[i]; current != null; current = current.next) {
                result.add(current.getKey());
            }
        }
        return result;
    }

    @Override
    public Collection<V> values() {
        List<V> list = new ArrayList<>();
        Map<K,V> map = new HashMap<>();
        for (int i = 0; i < capacity; i++) {
            for (Node<K,V> current = nodes[i]; current != null; current = current.next) {
                map.put(current.getKey(), current.getValue());
            }
        }
        for (Entry<K,V> maps : map.entrySet()) {
            list.add(maps.getValue());
        }
        return list;
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        Set<Entry<K,V>> result = new MyHashSet<>(capacity);
        for (int i = 0; i < capacity; i++) {
            for (Node<K,V> current = nodes[i]; current != null; current = current.next) {
               result.add(current);
            }
        }
        return result;
    }
}
