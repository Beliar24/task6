package com.java.customlist;

import java.util.*;

public class MyHashSet<Element> implements Set<Element> {

    private final HashMap<Element,Object> map;
    private static final int DEFAULT_CAPACITY = 16;
    private static final int MAX_ARRAY_SIZE = Integer.MAX_VALUE - 8;

    public MyHashSet() {
        map = new HashMap<>(DEFAULT_CAPACITY);
    }

    public MyHashSet(int capacity) {
        map = new HashMap<>(capacity);
    }

    @Override
    public int size() {
        return map.size();
    }

    @Override
    public boolean isEmpty() {
        return map.size() == 0;
    }

    @Override
    public boolean contains(Object o) {
        return map.containsKey(o);
    }

    @Override
    public Iterator<Element> iterator() {
        return map.keySet().iterator();
    }

    @Override
    public Object[] toArray() {
        ArrayList<Element> list = new ArrayList<>();
        for (Map.Entry<Element, Object> set : map.entrySet()) {
            list.add(set.getKey());
        }
        return list.toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        int size = size();
        T[] r = a.length >= size ? a :
                (T[])java.lang.reflect.Array.newInstance(a.getClass().getComponentType(), size);
        Iterator<Element> it = iterator();

        for (int i = 0; i < r.length; i++) {
            if (! it.hasNext()) {
                if (a == r) {
                    r[i] = null;
                } else if (a.length < i) {
                    return Arrays.copyOf(r, i);
                } else {
                    System.arraycopy(r, 0, a, 0, i);
                    if (a.length > i) {
                        a[i] = null;
                    }
                }
                return a;
            }
            r[i] = (T)it.next();
        }
        return it.hasNext() ? finishToArray(r, it) : r;
    }

    private static <T> T[] finishToArray(T[] r, Iterator<?> it) {
        int i = r.length;
        while (it.hasNext()) {
            int cap = r.length;
            if (i == cap) {
                int newCap = cap + (cap >> 1) + 1;

                if (newCap - MAX_ARRAY_SIZE > 0)
                    newCap = hugeCapacity(cap + 1);
                r = Arrays.copyOf(r, newCap);
            }
            r[i++] = (T)it.next();
        }

        return (i == r.length) ? r : Arrays.copyOf(r, i);
    }

    private static int hugeCapacity(int minCapacity) {
        if (minCapacity < 0)
            throw new OutOfMemoryError
                    ("Required array size too large");
        return (minCapacity > MAX_ARRAY_SIZE) ?
                Integer.MAX_VALUE :
                MAX_ARRAY_SIZE;
    }

    @Override
    public boolean add(Element element) {
        return map.put(element, null) == null;
    }

    @Override
    public boolean remove(Object o) {
        return map.remove(o) == null;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return c.containsAll(map.entrySet());
    }

    @Override
    public boolean addAll(Collection<? extends Element> c) {
        boolean condition = false;
        for (Element e : c)
            if (add(e))
                condition = true;
        return condition;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        boolean condition = false;
        Iterator<Element> it = iterator();
        while (it.hasNext()) {
            if (!c.contains(it.next())) {
                it.remove();
                condition = true;
            }
        }
        return condition;
    }

    public Element get(int index) {
        int count = 0;
        Element element = null;
        for (Map.Entry<Element, Object> maps : map.entrySet()) {
            if (index == count) {
                element = maps.getKey();
            }
            count++;
        }
        return element;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        boolean Condition = false;

        if (size() > c.size()) {
            for (Object element : c)
                Condition |= remove(element);
        } else {
            for (Iterator<?> i = iterator(); i.hasNext(); ) {
                if (c.contains(i.next())) {
                    i.remove();
                    Condition = true;
                }
            }
        }
        return Condition;
    }

    @Override
    public void clear() {
        map.clear();
    }
}
