package com.java.customlist;

import java.util.*;
import java.util.function.Consumer;

public class MyLinkedList<Element> implements List<Element>, Iterable<Element> {

    protected transient int modCount = 0;
    private Node<Element> first;
    private Node<Element> last;
    private int size;

    private static class Node<Element> {
        Element item;
        Node<Element> next;
        Node<Element> prev;

        public Node(Element item, Node<Element> next, Node<Element> prev) {
            this.item = item;
            this.next = next;
            this.prev = prev;
        }
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(Object o) {
        for (int i = 0; i < size; i++) {
            if (get(i) == o) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator<Element> iterator() {
        return new Iterator<Element>() {

            private Node<Element> current = first;

            @Override
            public boolean hasNext() {
                return current  != null;
            }

            @Override
            public Element next() {
                Node<Element> tmp = current;
                current = current.next;
                return tmp.item;
            }
        };
    }

    @Override
    public Object[] toArray() {
        Object[] result = new Object[size];
        int i = 0;
        for (Node<Element> x = first; x != null; x = x.next)
            result[i++] = x.item;
        return result;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        if (a.length < size)
            a = (T[])java.lang.reflect.Array.newInstance(a.getClass().getComponentType(), size);
        int i = 0;
        Object[] result = a;
        for (Node<Element> x = first; x != null; x = x.next)
            result[i++] = x.item;

        if (a.length > size)
            a[size] = null;

        return a;
    }

    @Override
    public boolean add(Element element) {
        MyLinkedList.Node<Element> node;
        if (first == null) {
            node = new MyLinkedList.Node<>(element, null, null);
            first = node;
        } else {
            node = new MyLinkedList.Node<>(element, null, last);
            last.next = node;
        }
        last = node;
        size++;
        return true;
    }

    @Override
    public boolean remove(Object o) {
        MyLinkedList.Node<Element> delete = getForValue((Element) o);
        if (indexOf(delete.item) == 0) {
            first = first.next;
        } else {
            MyLinkedList.Node<Element> before = delete.prev;
            MyLinkedList.Node<Element> after = delete.next;
            before.next = after;
            after.prev = before;
        }
        size--;
        return true;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object e : c)
            if (!contains(e))
                return false;
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends Element> c) {
        return addAll(size, c);
    }

    @Override
    public boolean addAll(int index, Collection<? extends Element> c) {

        Object[] a = c.toArray();
        int numNew = a.length;
        if (numNew == 0)
            return false;

        Node<Element> prev, succ;
        if (index == size) {
            succ = null;
            prev = last;
        } else {
            succ = node(index);
            prev = succ.prev;
        }

        for (Object o : a) {
            Element e = (Element) o;
            Node<Element> newNode = new Node<>(e, null, prev);
            if (prev == null)
                first = newNode;
            else
                prev.next = newNode;
            prev = newNode;
        }

        if (succ == null) {
            last = prev;
        } else {
            prev.next = succ;
            succ.prev = prev;
        }

        size += numNew;
        return true;
    }

    Node<Element> node(int index) {
        Node<Element> x;
        if (index < (size >> 1)) {
            x = first;
            for (int i = 0; i < index; i++)
                x = x.next;
        } else {
            x = last;
            for (int i = size - 1; i > index; i--)
                x = x.prev;
        }
        return x;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        boolean Condition = false;

        if (size() > c.size()) {
            for (Object element : c)
                Condition |= remove(element);
        } else {
            for (Iterator<?> i = iterator(); i.hasNext(); ) {
                if (c.contains(i.next())) {
                    i.remove();
                    Condition = true;
                }
            }
        }
        return Condition;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        if(c == null)
        {
            throw new NullPointerException("collection is null");
        }
        Iterator<Element> itr = iterator();

        boolean found = false;
        while(itr.hasNext())
        {
            if(!c.contains(itr.next()))
            {
                itr.remove();
                found = true;
            }
        }

        return found;
    }

    @Override
    public void clear() {
        for (Node<Element> x = first; x != null; ) {
            Node<Element> next = x.next;
            x.item = null;
            x.next = null;
            x.prev = null;
            x = next;
        }
        first = last = null;
        size = 0;
    }

    @Override
    public Element get(int index) {
        return getForIndex(index).item;
    }

    @Override
    public Element set(int index, Element element) {
        MyLinkedList.Node<Element> node = first;
        if (index == 0) {
            node.item = element;
        } else {
            getForIndex(index).item = element;
        }
        return element;
    }

    @Override
    public void add(int index, Element element) {
        if (index == 0) {
            MyLinkedList.Node<Element> node = new MyLinkedList.Node<>(element, first, null);
            MyLinkedList.Node<Element> tmpNode = first;
            tmpNode.prev = node;
            first = node;
            size++;
        }
        if (index == size - 1) {
            MyLinkedList.Node<Element> node = new MyLinkedList.Node<>(element, null, last);
            MyLinkedList.Node<Element> tmpNode = last;
            tmpNode.next = node;
            last = node;
            size++;
        }
        if (index > 0 && index < size - 1) {
            MyLinkedList.Node<Element> node = getForIndex(index);
            MyLinkedList.Node<Element> prevNode = node.prev;
            MyLinkedList.Node<Element> current = new MyLinkedList.Node<>(element, node.next, node.prev);
            prevNode.next = current;
            current.next = node;
            size++;
        }
    }

    @Override
    public Element remove(int index) {
        MyLinkedList.Node<Element> delete = getForIndex(index);
        if (indexOf(delete.item) == 0) {
            first = first.next;
        } else {
            MyLinkedList.Node<Element> before = delete.prev;
            MyLinkedList.Node<Element> after = delete.next;
            before.next = after;
            after.prev = before;
        }
        size--;
        return delete.item;
    }

    @Override
    public int indexOf(Object o) {
        MyLinkedList.Node<Element> node = first;
        int index = -1;
        for (int i = 0; i < size; i++) {
            if (node.item.equals(o)) {
                index = i;
            }
            node = node.next;
        }
        return index;
    }

    @Override
    public int lastIndexOf(Object o) {
        int index = size;
        if (o == null) {
            for (Node<Element> x = last; x != null; x = x.prev) {
                index--;
                if (x.item == null)
                    return index;
            }
        } else {
            for (Node<Element> x = last; x != null; x = x.prev) {
                index--;
                if (o.equals(x.item))
                    return index;
            }
        }
        return -1;
    }

    @Override
    public ListIterator<Element> listIterator() {
        return listIterator(0);
    }

    @Override
    public ListIterator<Element> listIterator(int index) {
        return new ListItr(index);
    }

    @Override
    public List<Element> subList(int fromIndex, int toIndex) {
        if  (fromIndex < 0 || toIndex > size() || fromIndex > toIndex)
            throw new IllegalArgumentException();

        ArrayList<Element> a = new ArrayList<>(toIndex - fromIndex);
        int i = 0;
        for (List<Element> l = this; last != null; i++) {
            if (i == toIndex)
                break;
            if (i >= fromIndex)
                a.add(first.item);
        }

        return Collections.unmodifiableList(a);
    }

    private MyLinkedList.Node<Element> getForIndex(int index) {
        MyLinkedList.Node<Element> result = first;
        for (int i = 0; i < index; i++) {
            result = result.next;
        }
        return result;
    }

    private MyLinkedList.Node<Element> getForValue(Element value) {
        MyLinkedList.Node<Element> result = first;
        for (int i = 0; i < size; i++) {
            if (result.item.equals(value)) {
                break;
            }
            result = result.next;
        }
        return result;
    }

    private class ListItr implements ListIterator<Element> {
        private Node<Element> lastReturned;
        private Node<Element> next;
        private int nextIndex;
        private int expectedModCount = modCount;

        ListItr(int index) {
            next = (index == size) ? null : node(index);
            nextIndex = index;
        }

        public boolean hasNext() {
            return nextIndex < size;
        }

        public Element next() {
            checkForComodification();
            if (!hasNext())
                throw new NoSuchElementException();

            lastReturned = next;
            next = next.next;
            nextIndex++;
            return lastReturned.item;
        }

        public boolean hasPrevious() {
            return nextIndex > 0;
        }

        public Element previous() {
            checkForComodification();
            if (!hasPrevious())
                throw new NoSuchElementException();

            lastReturned = next = (next == null) ? last : next.prev;
            nextIndex--;
            return lastReturned.item;
        }

        public int nextIndex() {
            return nextIndex;
        }

        public int previousIndex() {
            return nextIndex - 1;
        }

        public void remove() {
            checkForComodification();
            if (lastReturned == null)
                throw new IllegalStateException();

            Node<Element> lastNext = lastReturned.next;
            unlink(lastReturned);
            if (next == lastReturned)
                next = lastNext;
            else
                nextIndex--;
            lastReturned = null;
            expectedModCount++;
        }

        @Override
        public void set(Element element) {
            if (lastReturned == null)
                throw new IllegalStateException();
            checkForComodification();
            lastReturned.item = element;
        }

        void unlink(Node<Element> x) {
            final Element element = x.item;
            final Node<Element> next = x.next;
            final Node<Element> prev = x.prev;

            if (prev == null) {
                first = next;
            } else {
                prev.next = next;
                x.prev = null;
            }

            if (next == null) {
                last = prev;
            } else {
                next.prev = prev;
                x.next = null;
            }

            x.item = null;
            size--;
            modCount++;
        }

        @Override
        public void add(Element element) {
            checkForComodification();
            lastReturned = null;
            if (next == null)
                linkLast(element);
            else
                linkBefore(element, next);
            nextIndex++;
            expectedModCount++;
        }

        void linkLast(Element e) {
            final Node<Element> l = last;
            final Node<Element> newNode = new Node<>(e, null, l);
            last = newNode;
            if (l == null)
                first = newNode;
            else
                l.next = newNode;
            size++;
            modCount++;
        }

        void linkBefore(Element e, Node<Element> succ) {
            final Node<Element> pred = succ.prev;
            final Node<Element> newNode = new Node<>(e, succ, pred);
            succ.prev = newNode;
            if (pred == null)
                first = newNode;
            else
                pred.next = newNode;
            size++;
            modCount++;
        }

        public void forEachRemaining(Consumer<? super Element> action) {
            Objects.requireNonNull(action);
            while (modCount == expectedModCount && nextIndex < size) {
                action.accept(next.item);
                lastReturned = next;
                next = next.next;
                nextIndex++;
            }
            checkForComodification();
        }

        final void checkForComodification() {
            if (modCount != expectedModCount)
                throw new ConcurrentModificationException();
        }
    }

}
