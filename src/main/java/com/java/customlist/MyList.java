package com.java.customlist;

public interface MyList<T> {
    void add(T elem);

    void insert(T elem, int index);

    void remove(int index);

    void remove(T value);

    int indexOf(T value);

    boolean contains(T value);

    boolean containsAll(MyList<T> values);

    T get(int index);

    void set(int index, T value);

    int size();
}
