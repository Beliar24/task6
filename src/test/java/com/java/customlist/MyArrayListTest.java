package com.java.customlist;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class MyArrayListTest {

    MyList<String> array = new MyArrayList<>();

    @BeforeEach
    void setup() {
        array.add("Tesla");
        array.add("Camaro");
        array.add("Lexus");
        array.add("Audi");
        array.add("Infinity");
        array.add("Ford");
    }

    @Test
    void shouldBeCorrectSize() {
        assertEquals(array.size(), 6);
    }

    @Test
    void shouldBeCorrectRemoveAcrossValue() {
        array.remove("Tesla");
        assertEquals(array.size(), 5);
        assertEquals(array.get(0), "Camaro");
    }

    @Test
    void shouldBeCorrectRemoveAcrossIndex() {
        array.remove(2);
        assertEquals(array.size(), 5);
        assertEquals(array.get(2), "Audi");
    }

    @Test
    void shouldBeCorrectInsertMethod() {
        array.insert("Mustang", 2);
        assertEquals(array.size(), 7);
        assertEquals(array.get(2), "Mustang");
    }

    @Test
    void shouldBeCorrectIndexOfMethod() {
        assertEquals(array.indexOf("Camaro"), 1);
        assertEquals(array.indexOf("Camry"), -1);
    }

    @Test
    void shouldBeCorrectContainsMethod() {
        assertTrue(array.contains("Camaro"));
        assertFalse(array.contains("Camry"));
    }

    @Test
    void shouldBeCorrectContainsAllMethod() {
        MyList<String> arrayList = new MyArrayList<>();
        arrayList.add("Tesla");
        arrayList.add("Camaro");
        arrayList.add("Lexus");
        arrayList.add("Audi");
        arrayList.add("Infinity");
        arrayList.add("Ford");

        assertTrue(array.containsAll(arrayList));

        arrayList.add("Mustang");

        assertFalse(array.containsAll(arrayList));
    }

    @Test
    void shouldBeCorrectGetMethod() {
        assertEquals(array.get(3), "Audi");
    }

    @Test
    void shouldBeCorrectSetMethod() {
        array.set(2, "E-Tron");
        assertEquals(array.get(2), "E-Tron");
    }
}