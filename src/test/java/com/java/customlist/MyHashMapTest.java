package com.java.customlist;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class MyHashMapTest {

    Map<Integer, String> hash = new MyHashMap<>();

    @BeforeEach
    void setup() {
        hash.put(1, "Tesla");
        hash.put(2, "Camaro");
        hash.put(3, "Lexus");
        hash.put(4, "Audi");
        hash.put(5, "Infinity");
        hash.put(6, "Ford");
    }

    @Test
    void shouldBeCorrectSize() {
        assertEquals(hash.size(), 6);
    }

    @Test
    void shouldBeCorrectRemoveAcrossValue() {
        hash.remove(1);
        assertEquals(hash.size(), 5);
        assertEquals(hash.get(2), "Camaro");
    }

    @Test
    void shouldBeCorrectContainsKeyMethod() {
        assertTrue(hash.containsKey(1));
        assertFalse(hash.containsKey(8));
    }

    @Test
    void shouldBeCorrectContainsValueMethod() {
        assertTrue(hash.containsValue("Camaro"));
        assertFalse(hash.containsValue("Volvo"));
    }

    @Test
    void shouldBeCorrectPutAllMethod() {
        Map<Integer, String> hashMap = new MyHashMap<>();
        Map<Integer, String> copy = new MyHashMap<>();

        hashMap.put(1, "Tesla");
        hashMap.put(2, "Camaro");
        hashMap.put(3, "Lexus");
        hashMap.put(4, "Audi");
        hashMap.put(5, "Infinity");
        hashMap.put(6, "Ford");

        copy.putAll(hashMap);

        assertEquals(copy.size(), 6);
        assertEquals(copy.get(2), "Camaro");
    }

    @Test
    void shouldBeCorrectGetMethod() {
        assertEquals(hash.get(4), "Audi");
    }

    @Test
    void shouldBeCorrectIsEmptyMethod() {
        assertFalse(hash.isEmpty());
    }

    @Test
    void shouldBeCorrectClearMethod() {
        hash.clear();

        assertEquals(hash.size(), 0);
    }

}