package com.java.customlist;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class MyHashSetTest {

    MyHashSet<String> hashSet = new MyHashSet<>();

    @BeforeEach
    void setup() {
        hashSet.add("Tesla");
        hashSet.add("Camaro");
        hashSet.add("Lexus");
        hashSet.add("Audi");
        hashSet.add("Infinity");
        hashSet.add("Ford");
    }

    @Test
    void shouldBeCorrectSize() {
        assertEquals(hashSet.size(), 6);
    }

    @Test
    void shouldBeCorrectRemoveAcrossValue() {
        hashSet.remove("Tesla");
        assertEquals(hashSet.size(), 5);
        assertEquals(hashSet.get(0), "Camaro");
    }

    @Test
    void shouldBeCorrectContainsMethod() {
        assertTrue(hashSet.contains("Camaro"));
        assertFalse(hashSet.contains("Camry"));
    }

    @Test
    void shouldBeCorrectContainsAllMethod() {
        MyHashSet<String> hashSetList = new MyHashSet<>();
        hashSetList.add("Tesla");
        hashSetList.add("Camaro");
        hashSetList.add("Lexus");
        hashSetList.add("Audi");
        hashSetList.add("Infinity");
        hashSetList.add("Ford");

        assertTrue(hashSet.containsAll(hashSetList));

        hashSetList.add("Mustang");

        assertFalse(hashSet.containsAll(hashSetList));
    }

    @Test
    void shouldBeCorrectGetMethod() {
        assertEquals(hashSet.get(3), "Audi");
    }

    @Test
    void shouldBeCorrectIsEmptyMethod() {
        assertFalse(hashSet.isEmpty());
    }

    @Test
    void shouldBeCorrectToArrayMethod() {
        assertEquals(Arrays.toString(hashSet.toArray()), "[Camaro, Lexus, Infinity, Audi, Tesla, Ford]");
    }

    @Test
    void shouldBeCorrectRemoveAllMethod() {
        MyHashSet<String> hashSetList = new MyHashSet<>();
        hashSetList.add("Tesla");
        hashSetList.add("Camaro");
        hashSetList.add("Lexus");
        hashSetList.add("Audi");

        assertTrue(hashSet.removeAll(hashSetList));

        assertEquals(Arrays.toString(hashSet.toArray()), "[Infinity, Ford]");
    }

    @Test
    void shouldBeCorrectClearMethod() {
        hashSet.clear();

        assertEquals(hashSet.size(), 0);
    }

    @Test
    void shouldBeCorrectRetainAllMethod() {
        MyHashSet<String> hashSetList = new MyHashSet<>();
        hashSetList.add("Tesla");
        hashSetList.add("Camaro");
        hashSetList.add("Lexus");
        hashSetList.add("Audi");

        assertTrue(hashSet.retainAll(hashSetList));

        assertEquals(Arrays.toString(hashSet.toArray()), "[Camaro, Lexus, Audi, Tesla]");
    }
}