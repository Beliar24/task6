package com.java.customlist;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class MyLinkedListTest {

    List<String> linked = new MyLinkedList<>();

    @BeforeEach
    void setup() {
        linked.add("Tesla");
        linked.add("Camaro");
        linked.add("Lexus");
        linked.add("Audi");
        linked.add("Infinity");
        linked.add("Ford");
    }

    @Test
    void shouldBeCorrectSize() {
        assertEquals(linked.size(), 6);
    }

    @Test
    void shouldBeCorrectRemoveAcrossValue() {
        linked.remove("Tesla");
        assertEquals(linked.size(), 5);
        assertEquals(linked.get(0), "Camaro");
    }

    @Test
    void checkIterator() {
        List<String> listTmp = new ArrayList<>();
        listTmp.add("Tesla");
        listTmp.add("Camaro");
        listTmp.add("Lexus");
        listTmp.add("Audi");
        listTmp.add("Infinity");
        listTmp.add("Ford");

        for (int i = 0; i < linked.size(); i++) {
            assertEquals(linked.get(i), listTmp.get(i));
        }
    }

    @Test
    void shouldBeCorrectToArrayMethod() {
        assertEquals(Arrays.toString(linked.stream().sorted().toArray()), "[Audi, Camaro, Ford, Infinity, Lexus, Tesla]");
    }

    @Test
    void shouldBeCorrectRemoveAllMethod() {
        List<String> listTmp = new ArrayList<>();
        listTmp.add("Tesla");
        listTmp.add("Camaro");
        listTmp.add("Lexus");
        listTmp.add("Audi");

        assertTrue(linked.removeAll(listTmp));

        assertEquals(Arrays.toString(linked.toArray()), "[Infinity, Ford]");
    }

    @Test
    void shouldBeCorrectClearMethod() {
        linked.clear();

        assertEquals(linked.size(), 0);
    }

    @Test
    void shouldBeCorrectIsEmptyMethod() {
        assertFalse(linked.isEmpty());
    }

    @Test
    void shouldBeCorrectRemoveAcrossIndex() {
        linked.remove(2);
        assertEquals(linked.size(), 5);
        assertEquals(linked.get(2), "Audi");
    }

    @Test
    void shouldBeCorrectIndexOfMethod() {
        assertEquals(linked.indexOf("Camaro"), 1);
        assertEquals(linked.indexOf("Camry"), -1);
    }

    @Test
    void shouldBeCorrectContainsMethod() {
        assertTrue(linked.contains("Camaro"));
        assertFalse(linked.contains("Camry"));
    }

    @Test
    void shouldBeCorrectContainsAllMethod() {
        List<String> linkedList = new MyLinkedList<>();
        linkedList.add("Tesla");
        linkedList.add("Camaro");
        linkedList.add("Lexus");
        linkedList.add("Audi");
        linkedList.add("Infinity");
        linkedList.add("Ford");

        assertTrue(linked.containsAll(linkedList));

        linkedList.add("Mustang");

        assertFalse(linked.containsAll(linkedList));
    }

    @Test
    void shouldBeCorrectGetMethod() {
        assertEquals(linked.get(3), "Audi");
    }

    @Test
    void shouldBeCorrectSetMethod() {
        linked.set(2, "E-Tron");
        assertEquals(linked.get(2), "E-Tron");
    }

}